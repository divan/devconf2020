# MSSQL - Change Data Capture

This prototype uses Change Data Capture technology to creates JSON events.

The events are created by an insert trigger on the cdc.dbo.Account_CT table. This trigger creates a JSON version of teh data in the Account table and writes the JSON packet, along with meta data to the cdc.cdc_change_data table.

This the event is allowed to settle for a second before an entry is written to the queues.streaming_cdc table. This table is used to indicate to the dot net application that there is a record read to be processed.

The reason for the second delay before writing to the queue, is to allow for any non-deterministic behavior introduced by MSSQL triggers.

## Setup CDC

Step 1: Run all the db scripts in the create_db folder in numeric order, starting with 0_create_db.sql

Step 2: Run all the db scripts in the cdc folder in numeric order, starting with 0_Disable_Trigger_trgDDLEvent.sql

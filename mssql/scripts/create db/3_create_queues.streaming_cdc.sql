USE [MyOldBankingDB];

CREATE TABLE [queues].[streaming_cdc](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[QueueDate] [datetime] NOT NULL,
	[IsProcessing] [bit] NOT NULL,
	[Data] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_streaming_cdc] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [queues].[streaming_cdc] ADD  CONSTRAINT [DF_streaming_cdc_QueueDate]  DEFAULT (getdate()) FOR [QueueDate]
GO

ALTER TABLE [queues].[streaming_cdc] ADD  CONSTRAINT [DF_streaming_cdc_IsProcessing]  DEFAULT ((0)) FOR [IsProcessing]
GO
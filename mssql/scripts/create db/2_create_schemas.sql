USE [MyOldBankingDB];

IF NOT ((Select count(1) From sys.schemas Where name = 'queues') > 0)
BEGIN
	EXEC Sp_executesql N'CREATE SCHEMA queues'
END

IF NOT ((Select count(1) From sys.schemas Where name = 'cdc') > 0)
BEGIN
	EXEC Sp_executesql N'CREATE SCHEMA cdc'
END


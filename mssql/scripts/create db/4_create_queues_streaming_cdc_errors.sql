USE [MyOldBankingDB];

CREATE TABLE [queues].[streaming_cdc_errors](
	[Id] [bigint] NOT NULL,
	[QueueDate] [datetime] NOT NULL,
	[FailureDate] [datetime] NOT NULL,
	[Retries] [int] NOT NULL,
	[Data] [nvarchar](max) NOT NULL,
	[Error] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_streaming_cdc_errors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
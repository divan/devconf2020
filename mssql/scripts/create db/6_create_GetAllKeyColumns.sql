USE [MyOldBankingDB]
GO

CREATE FUNCTION [dbo].[GetAllKeyColumns] (
    @TableName Varchar(500)
)
RETURNS varchar(max)
AS
Begin

    Declare @ListColumns nvarchar(max)

    SELECT 
        @ListColumns = coalesce(@ListColumns + CHAR(13) +'''", "', ''' "') +  
		convert(varchar(200),COLUMN_NAME)+'":"''' + '	+ isnull(cast('+  
		convert(varchar(200),COLUMN_NAME)+' as varchar),'''')  + '
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = @TableName
    And SubString(COLUMN_NAME, Len(COLUMN_NAME) - 1, 2) = 'Id'
    and ORDINAL_POSITION > 5
    ORDER BY ORDINAL_POSITION

    RETURN @ListColumns

End;
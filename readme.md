# Event Streaming in a Legacy SQL DB: SQL meets Kafka and EventStore.

This is a demo project to prototype the ideas of using MSSQL Change Data Capture (CDC) technology to write events streams to Kafka and Event Store.

The prototype is made up of the following elements:

1. Docker Compose files: used to run the three main parts of the prototype:
2. MSSQL Scripts: Used to setup CDC on an MSSQL DB, any version from 2008 onwards.
3. Dot Net Core Application: A simple dot net core 2 application which acts as an Ambassador (or Proxy) between MSSQL and Kafka (or Event Store). Note the Dot Net application is designed to run in a Docker Container.
4. Node.js Application: A simple node.js application that consumes events from Kafka and projects them into a Postgres DB using an Upsert Function.
5. Postgres DB, with PostgREST UI: simple postgres database used to some projected report data which is projected from teh node.js application. The PostgREST API is configured to demonstrate the ease with which we can expose our data using modern technology stacks.

Each section directory of the five sections listed above have their own notes sections to get you started.

Please note that none of this code is production ready.
Parts of the code are taken from a production system, but since this is for demonstration the production robustness required from a solution like this has been removed.

A diagram of how the system hangs together is shown in the image below.

![alt text](./images/the_solution.png "Application Overview")

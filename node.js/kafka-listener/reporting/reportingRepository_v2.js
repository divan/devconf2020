const { Client } = require('pg');
const moment = require('moment');


function reportingRepository_v1(dbConnectionString) {

  var connectionString = dbConnectionString;

  this.init = function(dbConnectionString) {
    connectionString = dbConnectionString;
  };

  this.updateStats = function(account){

    const client = new Client(connectionString);
    (async () => {
      try{
        await client.connect();

        var accountOpenDate = moment(account.OpenDate);
        var year = accountOpenDate.year();
        var month = accountOpenDate.month() ; //+ 1;
        var product = account.ProductId;
        var originationChannel = account.OriginationChannelId;

        const queryString = 'Select public.update_regsiter_count_v2($1, $2, $3, $4)';
        const queryParameters = [year, month, product, originationChannel];
        
        await client.query(queryString, queryParameters)
          .then(res => console.log(res.rows[0]))
          .catch(e => console.error(e.stack));
        
      } catch {
        console.log('error writing to: ' + connectionString);
      } finally {
        await client.end()
      }
    })();

  };

}

module.exports = reportingRepository_v1;
 


const kafka = require('kafka-node');
const bp = require('body-parser');

function streamConsumer(dbRepository, streamConfig) {

    var repository = dbRepository;
    var config = streamConfig;

    const Consumer = kafka.Consumer;
    const client = new kafka.KafkaClient({kafkaHost: config.kafka_server});
      
    this.listenForEvents = function(listenerTopic, listenerGroupId, startFromOffset, offset){
  
        let consumer = new Consumer(
            client,
            [{ topic: listenerTopic, offset: 0, partition: 0 }],
            {
              groupId: listenerGroupId,
              autoCommit: true,
              fetchMaxWaitMs: 1000,
              fetchMaxBytes: 1024 * 1024,
              encoding: 'utf8',
              fromOffset: startFromOffset
            }
          );

          consumer.on('message', async function(message) {
            
            var eventValue = JSON.parse(message.value);
            
            console.log('kafka-> ', JSON.parse(message.value));

            dbRepository.updateStats(eventValue);
      
          });

          consumer.on('error', function(err) {
            console.log('error', err);
          });
  
    };
  
  }
  
  module.exports = streamConsumer;
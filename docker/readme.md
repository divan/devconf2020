# Docker 

The following folder contains 3 folders:

    1. kafka
    2. eventstore
    3. postgres

Each of these folders contains a docker-compose file to start the containers associated with parts of this demo.

To run the environments start the docker containers using the docker-compose command.

Navigate to the folder ./docker/kafka, ./docker/eventstore or ./docker/postgres.

Run the command "docker-compose up -d" , to start the containers.

    docker-compose up -d

The docker images used will automatically download, after which the docker containers will start.

Note: The download may take a while if you are on a slow data link.

To establish if the containers are running use the command:

    docker ps

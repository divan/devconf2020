# Kafka

## Using Kafka

Getting started:

https://dzone.com/articles/how-to-configure-an-apache-kafka-cluster-on-ubuntu

### Topics

#### Create a topic

https://kafka.apache.org/quickstart

Step 1: 
    Log into a kafka container  
    docker exec -it kafka_kafka_1 /bin/bash
Step 2:
    Go to bin folder
    cd /opt/kafka/bin
Step 2:
    Run the command
    kafka-topics.sh --zookeeper zookeeper:2181 --create --topic helloworld --partitions 3 --replication-factor 3

    kafka-topics.sh --zookeeper zookeeper:2181 --create --topic my-test --partitions 1 --replication-factor 1

    kafka-topics.sh --bootstrap-server localhost:9092 --create --replication-factor 1 --partitions 1 --topic some-test


Notes: Naming Topics
Preference is to follow general naming conventions that you would follow
when using Unix/Linux. You will probably avoid a lot of frustrating errors and warnings if you
do not have spaces or various special characters that do not always play nice with what you
really want to do.

#### List topics

kafka-topics.sh --zookeeper zookeeper:2181 --list

#### View topic layouts

kafka-topics.sh --describe --zookeeper zookeeper:2181 --topic helloworld
kafka-topics.sh --describe --zookeeper zookeeper:2181 --topic my-test

#### Send test message

kafka-console-producer.sh --broker-list localhost:9092 --topic my-test

#### Read a message

kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic my-test --from-beginning


You really can see that it is not like a traditional single topic in other systems.
The numbers you see under by the leader, replicas, and isr fields are the broker.id that we set
in our configuration files. Briefly looking at the output, for example, we can see that our broker
id of 0 is the leader for partition 0. Partition 0 also has replicas that exist on brokers 1 and 2. The
last column "isr" stands for in-sync replicas.

#### Remove a topic

kafka-topics.sh --zookeeper zookeeper:2181 --delete --topic helloworld

#### Auto Create Topic

There is configuration to enable or disable auto-creation of topics. However, I
prefer to control the creation of topics as a specific action and do not want any new topics to
randomly show up if I mistype a topic name once or twice or be recreated due to producer
retires.

## Zookeeper

### List brokers

zookeeper-shell.sh localhost:2181 ls /brokers/topics

### Producers
Used to send messages
As mentioned in our use cases in Chapter 1, a good example is log files that are produced from an application.

A producer is also a way to send messages inside Kafka itself. For
example, if you are reading data from a specific topic and wanted to send it to a different topic,
you would also use a producer.

### Consumers
In contrast, a Consumer is the way to retrieve messages from Kafka. In the same vein as
producers, if you are talking about getting data out of Kafka, you are looking at consumers being
involved directly or somewhere in the process.

## Kafka Architecture
One of the keys for Kafka is its usage of the pagecache of the operating system. By avoiding
caching in the JVM heap, the brokers can help avoid some of the issues that large heaps can
hold, ie. long or frequent garbage collection pauses.

While new messages flood in, it is likely that the latest messages would be of
most interest to many consumers which could then be served from this cache.

In most cases, adding more RAM will help more of your workload work fall into the pagecache.

As mentioned before, Kafka uses its own protocol - AMQP was noted by the creators as having too large a part in the impacts

#### Commit Logs
What makes the commit log special is its append only nature in which events are added to the
end. In most traditional systems, linear read and writes usually perform better than random
operations that would require spinning disks. The persistence as a log itself for storage is a major
part of what separates Kafka from other message brokers. Reading a message does not remove it
from the system or exclude it from other consumers.

## Sample Kafka Code 

Kafka Dot Net 

https://docs.confluent.io/current/clients/dotnet.html
https://cwiki.apache.org/confluence/display/KAFKA/Clients#Clients-.NET
https://github.com/confluentinc/confluent-kafka-dotnet
https://github.com/confluentinc/confluent-kafka-dotnet/tree/master/examples
https://github.com/confluentinc/confluent-kafka-dotnet#working-with-apache-avro
https://github.com/confluentinc/confluent-kafka-dotnet/blob/master/examples/Producer/Program.cs
https://github.com/confluentinc/confluent-kafka-dotnet/blob/master/examples/Consumer/Program.cs
https://github.com/confluentinc/confluent-kafka-dotnet/blob/master/examples/AvroGeneric/Program.cs

﻿using System;

namespace LegacyEventStreaming.Mssql.ChangeData
{
    public class ChangeDataRecord : IChangeDataRecord
    {
        public long Id { get; set; }
        public long QueueId { get; set; }
        public DateTime ChangeDate { get; set; }
        public byte[] LSN { get; set; }
        public byte[] LSNSV { get; set; }
        public string SourceDatabase { get; set; }
        public string SourceSchema { get; set; }
        public string SourceTable { get; set; }
        public string Operation { get; set; }
        public string Json { get; set; }
    }
}
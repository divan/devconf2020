﻿using System;

namespace LegacyEventStreaming.Mssql.ChangeData
{
    public class QueuedChangeDataRecord
    {
        public long Id { get; set; }
        public DateTime QueueDate { get; set; }
        public bool IsProcessing { get; set; }
        public string Data { get; set; }
    }
}
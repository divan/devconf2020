﻿using System.Threading.Tasks;
using LegacyEventStreaming.Common;
using LegacyEventStreaming.Common.EventStreaming;

namespace LegacyEventStreaming.Streaming
{
    public interface IEventProducer
    {
        Task<bool> HandleEvent(string stream, StreamingEvent streamingEvent);
    }
}
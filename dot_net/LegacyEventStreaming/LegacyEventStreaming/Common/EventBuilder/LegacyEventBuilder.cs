﻿using LegacyEventStreaming.BusinessDomain.Account;
using LegacyEventStreaming.Common.EventStreaming;
using LegacyEventStreaming.Common.Serialisation;
using LegacyEventStreaming.Mssql.ChangeData;
using Newtonsoft.Json;

namespace LegacyEventStreaming.Common.EventBuilder
{
    public class LegacyEventBuilder<T> : IEventBuilder where T : ILegacyEvent 
    {
        public StreamingEvent CreateEvent(IChangeDataRecord changeDataRecord)
        {
            T legacyEvent = JsonConvert.DeserializeObject<T>(changeDataRecord.Json, new LegacyEventConverter<T>());
            legacyEvent.Action = changeDataRecord.Operation;
            string value = JsonConvert.SerializeObject(legacyEvent);;
            return new StreamingEvent() { Key = changeDataRecord.Id.ToString(), Value = value, EventType = changeDataRecord.SourceTable};
        }
    }
}
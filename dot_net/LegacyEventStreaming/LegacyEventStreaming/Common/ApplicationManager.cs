﻿using System;
using System.Threading;

namespace LegacyEventStreaming.Common
{
    public static class ApplicationManager
    {
        public static CancellationTokenSource SetupCancellationToken()
        {
            CancellationTokenSource cancellationToken = new CancellationTokenSource();

            AppDomain.CurrentDomain.ProcessExit += (object sender, EventArgs e) =>
            {
                cancellationToken.Cancel();
            };

            Console.CancelKeyPress += (object sender, ConsoleCancelEventArgs e) =>
            {
                var isCtrlC = e.SpecialKey == ConsoleSpecialKey.ControlC;
                var isCtrlBreak = e.SpecialKey == ConsoleSpecialKey.ControlBreak;

                if (isCtrlC) { e.Cancel = true; cancellationToken.Cancel();; }
                else if (isCtrlBreak) { cancellationToken.Cancel();; }

            };

            return cancellationToken;
        }
    }
}
--Setup app user  

create role app_user noinherit login password 'password';

grant usage on schema public to app_user;
grant usage on schema api to app_user;

--grant All to app_user;
